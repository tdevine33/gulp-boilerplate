# Ligature Gulp.js Boilerplate

Welcome to the wonderful world of automation! To use the gulp.js file, run the following command in order to install all necessary dependencies.

`npm install`

Once dependencies are installed, simply run `gulp` from the command line to run the build process.

`gulp`

## Images

- imagemin

## Scripts

- concat
- deporder
- stripdebug
- uglify

## Styles

- postcss
- autoprefixer
- mqpacker
- cssnano

## Watch

- livereload