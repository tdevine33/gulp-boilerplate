const gulp = require('gulp');
const newer = require('gulp-newer');
const livereload = require('gulp-livereload');
const devBuild = (process.env.NODE_ENV !== 'production');
const folder = {
	src: 'src/',
	build: 'build/',
};

//********************
// IMAGE MIN
//********************
const imagemin = require('gulp-imagemin');

function images(cb) {
	const out = folder.build + 'img/';

	return gulp.src(folder.src + 'img/**/*')
		.pipe(newer(out))
		.pipe(imagemin({ optimizationLevel: 5 }))
		.pipe(gulp.dest(out));

	cb();
}

//********************
// SCRIPTS
//********************
const concat = require('gulp-concat');
const deporder = require('gulp-deporder');
const stripdebug = require('gulp-strip-debug');
const uglify = require('gulp-uglify');

function scripts(cb) {
	const jsbuild = gulp.src(folder.src + 'js/**/*')
		.pipe(deporder())
		.pipe(concat('main.js'));

	if(!devBuild) {
		jsbuild = jsbuild
			.pipe(stripdebug())
			.pipe(uglify());
	}

	return jsbuild
		.pipe(gulp.dest(folder.build + 'js/'))
		.pipe(livereload());
	
	cb();
}

//********************
// STYLES
//********************
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const assets = require('postcss-assets');
const autoprefixer = require('autoprefixer');
const mqpacker = require('css-mqpacker');
const cssnano = require('cssnano');

function styles(cb) {
	var postCssOpts = [
		assets({ loadPaths: ['images/'] }),
		autoprefixer({ browsers: ['last 2 versions', '> 2%'] }),
		mqpacker
	];

	if(!devBuild){
		postCssOpts.push(cssnano);
	}

	return gulp.src(folder.src + 'scss/style.scss')
		.pipe(sass({
			outputStyle: 'nested',
			imagePath: 'images/',
			precision: 3,
			errLogToConsole: true
		}))
		.pipe(postcss(postCssOpts))
		.pipe(gulp.dest(folder.build + 'css/'))
		.pipe(livereload());

	cb();
}

function reload(cb) {
	livereload.listen();
	cb();
}

//********************
// DEFAULT
//********************
gulp.task('default', gulp.parallel(images, scripts, styles));

//********************
// WATCH
//********************

gulp.watch(folder.src + '**/*.js', gulp.series(scripts, reload));
gulp.watch(folder.src + '**/*.scss', gulp.series(styles, reload));
// gulp.watch(folder.src + '**/*.scss', styles);
// gulp.watch(folder.src + '**/*.js', scripts);